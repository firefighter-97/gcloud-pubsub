package de.redelin;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
public class PubSubConfig {

  /**
   * instantiate the input channel, which will be used by ServiceActivators
   */
  @Bean
  public MessageChannel pubsubInputChannel() {
    return new DirectChannel();
  }
}
