package de.redelin;

import lombok.Data;

@Data
public class HolyMessage {
	private String text;
	private int id;
	private String directionKey;
}
