package de.redelin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gcp.pubsub.core.PubSubOperations;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.pubsub.v1.AckReplyConsumer;

/**
 * this consumer reacts on configuration based on Sink.class (input constant is used to be
 * identified in application.yml)
 * 
 * @author sredelin
 *
 */
@Service
public class DataConsumer {
  final Logger log = LoggerFactory.getLogger(DataConsumer.class);

  @Value("${spring.cloud.stream.bindings.input.destination}")
  String topic;

  List<HolyMessage> erroneousMessages = new ArrayList<>();

  /**
   * read and process message
   */
  @Bean
  @ServiceActivator(inputChannel = "pubsubInputChannel")
  public MessageHandler messageReceiver() {
    return message -> {
      /*
       * message with id 2 should not be processable at first try, so we can see an error message in
       * processing and how googles pub/sub and spring cloud data flow react. Exception-Handling is
       * bad in this implementation, because normally as example an error-topic should be used.
       */
      log.info("Message arrived! Payload: " + message.getPayload());
      HolyMessage m;
      try {
        m = new ObjectMapper().readValue(message.getPayload().toString(), HolyMessage.class);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      if (m.getId() == 2 && !erroneousMessages.contains(m)) {
        log.error("ID 2 not legal!");
        erroneousMessages.add(m);
        throw new IllegalArgumentException("My Exception");
      }

      AckReplyConsumer consumer =
          (AckReplyConsumer) message.getHeaders().get(GcpPubSubHeaders.ACKNOWLEDGEMENT);
      consumer.ack();
    };
  }

  /**
   * instantiate the input channel on defined topic
   */
  @Bean
  public PubSubInboundChannelAdapter messageChannelAdapter(
      @Qualifier("pubsubInputChannel") MessageChannel inputChannel,
      PubSubOperations pubSubTemplate) {
    PubSubInboundChannelAdapter adapter = new PubSubInboundChannelAdapter(pubSubTemplate, topic);
    adapter.setOutputChannel(inputChannel);
    adapter.setAckMode(AckMode.MANUAL);

    return adapter;
  }
}
