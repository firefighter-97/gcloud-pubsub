Spring Cloud Data Flow with GCP Pub/Sub example
============
This example will show you how to configure your spring boot application against a Google Cloud Pub/Sub topic.  
For google cloud platform (GCP) this is an alternative to a self or third party hosted kafka event driven architecture.  
The following requirements were important for messaging broker decision:  
1. broker organized its data stream in topics  
2. each topic can have multiple subscribers (in kafka groups), which can consume data in parallel as well as unique  
3. spring cloud data flow is adaptable to this broker  

# the structure
## SpringDataFlowSource => the source of holy data
The subdirectory is a single gradle project and can be executed by running `./gradlew bootRun`.  
It is starting with port 8080 and a message can be triggered by calling [http://localhost:8080/source/forward?message=Testmessage](http://localhost:8080/source/forward?message=Testmessage).  
The controller will forward the data to `DataProducer.java` and the producer will send the data to pub/sub topic.  
In the server log you can see log messages like:  
`2018-06-18 23:18:46.337  INFO 74008 --- [nio-8080-exec-3] de.redelin.Controller                    : Send message: {"text":"Testmessage","id":0,"directionKey":"A"}`

## SpringDataFlowProcess => the sink of holy data
The subdirectory is a single gradle project and can be executed by running `./gradlew bootRun`.  
It is starting with port 9081.  
The `DataConsumer.java` will receive the data from pub/sub topic, parse the payload to `HolyMessage` object and acknowledge the message if successfull processed.  
In the server log you can see log messages like:  
`2018-06-18 23:18:46.834  INFO 70360 --- [pool-2-thread-1] de.redelin.DataConsumer                  : Message arrived! Payload: {"text":"Testmessage","id":8,"directionKey":"A"}`

*Attention*: If a exception occurs and the message has not been acknowledged, the message will be tried to reprocess multiple times in an endless loop and block the counter of the subscriber in the topic.  
In this example you can see, that message with id = 2 will not be processed at first try, but at second try. In server log you can see a exception while processing and a few seconds later a successful processing, because the consumer only throws an exception at the first try of this message in this consumer.

# configuration at google cloud platform (GCP)
1. open GCP console [https://console.cloud.google.com](https://console.cloud.google.com)  
2. open GCP app "Credentials" of "API & Services" using search bar  
3. create Credentials >> Service account key
4. choose a "app engine default service account" (If you want to have a unique user account for the consumer choose "new service account")  
5. store the json-private key in both java services under src/main/resources/gcloud-key.json  
6. update `application.yml` in both java services in line `project-id: "yourproject"` with your GCP project ID  
7. open GCP app "Pub/Sub" using search bar  
8. create a topic with name "testtopic2" (only for this showcase)  
9. enter configuration of this topic at console (click on it)  
10. create a subscription with name "testsubscription2" using DeliveryType "Pull"  
11. change permission on topic and add your choosen service account as role "Pub/Sub Publisher" and "Pub/Sub Subscriber" (For test we are using the same account to publish and subscribe)  


## findings
- you have to design a retry-process for failing messages, if you don't want to lock the processing  
- throwing exceptions in consumer-event leads to locking process and not acknowleding messages  
- old messages with non parseable objects lead to big problems for new consumer-groups  
- two consumers on the same subscription will get unique message, double processing is prevented by pub/sub  
- DeliveryType "Pull" can be used by spring cloud dataflow to receive message as events in a running process  
- DeliveryType "Push" can execute a external URL endpoint and can start processes which will consume data afterwards. This can be very helpful for processes which are not consuming data whole day.