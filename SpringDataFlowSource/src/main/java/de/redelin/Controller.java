package de.redelin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.redelin.DataProducer.PubsubOutboundGateway;

/**
 * this controller is used to start a process by incoming message
 * 
 * @author sredelin
 *
 */
@RestController
@RequestMapping("/source")
public class Controller {

  final Logger log = LoggerFactory.getLogger(Controller.class);

  @Autowired
  private PubsubOutboundGateway messagingGateway;

  @RequestMapping(method = RequestMethod.GET, value = "/forward")
  public String sendMessage(@RequestParam(name = "message", required = true) String message)
      throws JsonProcessingException {
    /*
     * produce 20 messages with unique combination of id and directionKey and send them to producer
     */
    for (int i = 0; i < 10; i++) {
      HolyMessage m = HolyMessage.builder().text(message).id(i).directionKey("A").build();
      String payload = new ObjectMapper().writeValueAsString(m);
      messagingGateway.sendToPubsub(payload);
      log.info("Send message: {}", payload);
    }
    return "sent";
  }
}
