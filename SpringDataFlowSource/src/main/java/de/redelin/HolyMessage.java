package de.redelin;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class HolyMessage {
	private String text;
	private int id;
	private String directionKey;
}
